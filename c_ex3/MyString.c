//
//  MyString.c
//


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "MyString.h"

/**
 * struct for MyString
 */
struct _MyString
{
    char * _sptr; // pointer to c string
    int _length; // length of string
};

static void freeInsideString(MyString * str)
{
    if(str->_sptr != NULL)
    {
        free(str->_sptr);
        str->_sptr = NULL;
        
    }
}

static MyStringRetVal changeMyStringLength(MyString * str, const int length)
{
    // free old data pointed by sptr
    if(str != NULL)
    {
        freeInsideString(str);
    }
    // allocate memory for the string data
    str->_sptr = (char*) malloc(length);
    
    if(str->_sptr == NULL)
    {
        // if malloc failed
        return MYSTRING_ERROR;
    }
    
    // update length property of the new string
    str->_length = length;
    
    return MYSTRING_SUCCESS;
}


#ifndef NDEBUG

static void testMsg(int testNum, char * funcName, char * testTitle)
{
    printf("\nTest no.\t\t%03d\nFunction Name\t\t%s\nTest Title\t\t%s\n",
           testNum, funcName, testTitle);
}


static void failMsg()
{
    printf("Failed\n");
}


static void passMsg()
{
    printf("Passed\n");
}

/**
 * test
 */
static void test1()
{
    int testNum = 1;
    testMsg(testNum, "myStringAlloc", "Check if newlly allocated MyString has length 0");
    MyString * str = myStringAlloc();
    if(str->_length != 0)
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
}

/**
 * test
 */
static void test2()
{
    /*
    int testNum = 2;
    testMsg(testNum, "myStringFree", "Check if input str->_sptr is NULL at the end");
    MyString * str = myStringAlloc();
    myStringFree(str);
    if(str->_sptr != NULL)
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
     */
}

/**
 * test
 */
static void test3()
{
    int testNum = 3;
    testMsg(testNum, "myStringClone", "Check if str and str2 starts with the same char");
    MyString * str = myStringAlloc();
    changeMyStringLength(str, 1);
    str->_sptr[0] = 'a';
    MyString * str2 = myStringClone(str);
    if(str2->_sptr[0] != str->_sptr[0])
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
    myStringFree(str2);
}

/**
 * test
 */
static void test4()
{
    int testNum = 4;
    testMsg(testNum, "myStringClone", "Check if str and str2 are of the same length");
    MyString * str = myStringAlloc();
    changeMyStringLength(str, 1);
    str->_sptr[0] = 'a';
    MyString * str2 = myStringClone(str);
    if(str2->_length != str->_length)
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
    myStringFree(str2);
}

/**
 * test
 */
static void test5()
{
    int testNum = 5;
    testMsg(testNum, "myStringSetFromMyString", "Check if str and str2 are of the same length");
    MyString * str = myStringAlloc();
    changeMyStringLength(str, 1);
    str->_sptr[0] = 'a';
    MyString * str2 = myStringAlloc();
    myStringSetFromMyString(str2, str);
    if(str2->_length != str->_length)
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
    myStringFree(str2);
}

/**
 * test
 */
static void test6()
{
    int testNum = 6;
    testMsg(testNum, "myStringSetFromMyString", "Check if str and str2 starts with the same char");
    MyString * str = myStringAlloc();
    changeMyStringLength(str, 1);
    str->_sptr[0] = 'a';
    MyString * str2 = myStringAlloc();
    myStringSetFromMyString(str2, str);
    if(str2->_sptr[0] != str->_sptr[0])
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
    myStringFree(str2);
}


/**
 * test filter
 */
bool filt1(const char * c)
{
    if(*c == 'a')
    {
        return true;
    }
    else
    {
        return false;
    }
}

typedef bool (*filt)(const char * c);


/**
 * test
 */
static void test7()
{
    int testNum = 7;
    testMsg(testNum, "myStringFilter", "check if length is true after filter");
    MyString * str = myStringAlloc();
    changeMyStringLength(str, 1);
    str->_sptr[0] = 'a';
    filt func = filt1;
    myStringFilter(str, func);
    if(str->_length != 0)
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
}

/**
 * test
 */
static void test8()
{
    int testNum = 8;
    testMsg(testNum, "myStringFilter", "check if length is true after filter");
    MyString * str = myStringAlloc();
    changeMyStringLength(str, 2);
    str->_sptr[0] = 'a';
    str->_sptr[1] = 'b';
    filt func = filt1;
    myStringFilter(str, func);
    if(str->_length != 1)
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
}

/**
 * test
 */
static void test9()
{
    int testNum = 9;
    testMsg(testNum, "myStringSetFromCString", "check if first char in str is the same as the c string");
    MyString * str = myStringAlloc();
    myStringSetFromCString(str, "abc");
    if(str->_sptr[0] != 'a')
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
}

/**
 * test
 */
static void test10()
{
    int testNum = 10;
    testMsg(testNum, "myStringSetFromInt", "check if for input 123 first char is '1'");
    MyString * str = myStringAlloc();
    myStringSetFromInt(str, 123);
    if(str->_sptr[0] != '1')
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
}

/**
 * test
 */
static void test11()
{
    int testNum = 11;
    testMsg(testNum, "myStringToInt", "check if function returns 123 for str '123'");
    MyString * str = myStringAlloc();
    myStringSetFromInt(str, 123);
    if(myStringToInt(str) != 123)
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
}

/**
 * test
 */
static void test12()
{
    int testNum = 12;
    testMsg(testNum, "myStringToCString", "check if function returns str with first char '1' for str '123'");
    MyString * str = myStringAlloc();
    myStringSetFromInt(str, 123);
    char * cstr = myStringToCString(str);
    if(cstr[0] != '1')
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    free(cstr);
    myStringFree(str);
}

/**
 * test
 */
static void test13()
{
    int testNum = 13;
    testMsg(testNum, "myStringCat", "do 'abc' + 'def' and check first and fourth chars");
    MyString * str = myStringAlloc();
    MyString * str2 = myStringAlloc();
    myStringSetFromCString(str, "abc");
    myStringSetFromCString(str2, "def");
    myStringCat(str, str2);
    if(str->_sptr[3] != 'd' || str->_sptr[0] != 'a')
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
    myStringFree(str2);
}


/**
 * test
 */
static void test14()
{
    int testNum = 14;
    testMsg(testNum, "myStringCatTo", "do 'abc' + 'def' and check first and fourth chars");
    MyString * str = myStringAlloc();
    MyString * str2 = myStringAlloc();
    MyString * str3 = myStringAlloc();
    myStringSetFromCString(str, "abc");
    myStringSetFromCString(str2, "def");
    myStringCatTo(str, str2, str3);
    if(str3->_sptr[3] != 'd' || str3->_sptr[0] != 'a')
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
    myStringFree(str2);
    myStringFree(str3);
}

/**
 * test
 */
static void test15()
{
    int testNum = 15;
    testMsg(testNum, "myStringCompare", "check if two identical string returns 0");
    MyString * str = myStringAlloc();
    MyString * str2 = myStringAlloc();
    myStringSetFromInt(str, 123);
    myStringSetFromInt(str2, 123);
    if(myStringCompare(str, str2) != 0)
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
    myStringFree(str2);
}

/**
 * test
 */
static void test16()
{
    int testNum = 16;
    testMsg(testNum, "myStringCompare", "check if two different string returns not 0");
    MyString * str = myStringAlloc();
    MyString * str2 = myStringAlloc();
    myStringSetFromInt(str, 14);
    myStringSetFromInt(str2, 123);
    if(myStringCompare(str, str2) == 0)
    {
        failMsg();
    }
    else
    {
        passMsg();
    }
    myStringFree(str);
    myStringFree(str2);
}


int main()
{
    // Run all tests
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    test9();
    test10();
    test11();
    test12();
    test13();
    test14();
    test15();
    test16();
    return EXIT_SUCCESS;
}

#endif

/**
 * @brief Allocates a new MyString and sets its value to "" (the empty string).
 * 			It is the caller's responsibility to free the returned MyString.
 *
 * RETURN VALUE:
 * @return a pointer to the new string, or NULL if the allocation failed.
 */
MyString * myStringAlloc()
{
    MyString * str = (MyString*) malloc(sizeof(MyString));
    if (str != NULL)
    {
        // configure empty string
        str->_length = 0;
        str->_sptr = (char *) malloc(0);
    }
    else
    {
        printf("Error: No memory for string");
    }
    return str;
}

/**
 * @brief Frees the memory and resources allocated to str.
 * @param str the MyString to free.
 * If str is NULL, no operation is performed.
 */
void myStringFree(MyString *str)
{
    if (str != NULL)
    {
        freeInsideString(str);
        free(str);
    }
}

/**
 * @brief Allocates a new MyString with the same value as str. It is the caller's
 * 			responsibility to free the returned MyString.
 * @param str the MyString to clone.
 * RETURN VALUE:
 *   @return a pointer to the new string, or NULL if the allocation failed.
 */
MyString * myStringClone(const MyString *str)
{
    int length = str->_length;
    MyString * str2 = myStringAlloc();
    if(str2 != NULL)
    {
        if (length > 0)
        {
            myStringSetFromMyString(str2, str);
        }
    }
    return str2;
}

/**
 * @brief Sets the value of str to the value of other.
 * @param str the MyString to set
 * @param other the MyString to set from
 * RETURN VALUE:
 *  @return MYSTRING_SUCCESS on success, MYSTRING_ERROR on failure.
 */
MyStringRetVal myStringSetFromMyString(MyString *str, const MyString *other)
{
    int length = other->_length;
    
    // check if str is initialized
    if(str == NULL)
    {
        // str is not pointing to MyString
        return MYSTRING_ERROR;
    }
    
    // make strings equal in length
    if (str->_length != length)
    {
        if(changeMyStringLength(str, length) == MYSTRING_ERROR)
        {
            return MYSTRING_ERROR;
        }
    }
    
    // copy data to new string
    memcpy(str->_sptr, other->_sptr, length);
    return MYSTRING_SUCCESS;
}

/**
 * @brief filter the value of str acording to a filter.
 * 	remove from str all the occurrence of chars that are filtered by filt
 *	(i.e. filr(char)==true)
 * @param str the MyString to filter
 * @param filt the filter
 * RETURN VALUE:
 *  @return MYSTRING_SUCCESS on success, MYSTRING_ERROR on failure. */
MyStringRetVal myStringFilter(MyString *str, bool (*filt)(const char *))
{
    // count how many chars returns true when passed to filt
    int counter = 0;
    for (int i = 0; i < str->_length; i++)
    {
        if(filt(str->_sptr + i) == true)
        {
            counter++;
        }
    }
    
    char * newStr = (char *) malloc(str->_length - counter);
    
    // if allocation failed return error
    if (newStr == NULL)
    {
        return MYSTRING_ERROR;
    }
    
    // add values to new array
    int counter2 = 0;
    for (int i = 0; i < str->_length; i++)
    {
        if(filt(str->_sptr + i) == true)
        {
            counter2++;
        }
        else
        {
            newStr[i-counter2] = str->_sptr[i];
        }
    }
    
    // free old array's memory and point sptr to the new one
    freeInsideString(str);
    str->_length -= counter2;
    str->_sptr = newStr;
    
    return MYSTRING_SUCCESS;
}

/**
 * @brief Sets the value of str to the value of the given C string.
 * 			The given C string must be terminated by the null character.
 * 			Checking will not use a string without the null character.
 * @param str the MyString to set.
 * @param cString the C string to set from.
 * RETURN VALUE:
 *  @return MYSTRING_SUCCESS on success, MYSTRING_ERROR on failure.
 */
MyStringRetVal myStringSetFromCString(MyString *str, const char * cString)
{
    int length = 0;
    while(cString[length] != '\0')
    {
        length++;
    }
    if(changeMyStringLength(str, length) == MYSTRING_ERROR)
    {
        return MYSTRING_ERROR;
    }
    memcpy(str->_sptr, cString, length);
    return MYSTRING_SUCCESS;
}

/**
 * @brief Sets the value of str to the value of the integer n.
 *	(i.e. if n=7 than str should contain ‘7’)
 * 	You are not allowed to use itoa (or a similar functions) here but must code your own conversion function.
 * @param str the MyString to set.
 * @param n the int to set from.
 * RETURN VALUE:
 *  @return MYSTRING_SUCCESS on success, MYSTRING_ERROR on failure.
 */
MyStringRetVal myStringSetFromInt(MyString *str, int n)
{
    int length = 1;
    int m = n;
    while (m >= 10) // m has more than one digit
    {
        m /= 10;
        length++;
    }
    if(changeMyStringLength(str, length) == MYSTRING_ERROR)
    {
        return MYSTRING_ERROR;
    }
    for(; length > 0; length--)
    {
        str->_sptr[length - 1] = (char) n % 10 + '0';
        n /= 10;
    }
    return MYSTRING_SUCCESS;
}

/**
 * @brief Returns the value of str as an integer.
 * 	If str cannot be parsed as an integer,
 * 	the return value should be MYSTR_ERROR_CODE
 * 	NOTE: positive and negative integers should be supported.
 * @param str the MyString
 * @return an integer
 */
int myStringToInt(const MyString *str)
{
    int output = 0;
    int currentFactor = 1;
    for (int i = str->_length - 1; i >= 0; i--)
    {
        char c = str->_sptr[i];
        if((c < '0') && (c > '9'))
        {
            return MYSTRING_ERROR;
        }
        output +=  ((int) c - '0') * currentFactor;
        currentFactor *= 10;
    }
    return output;
}

/**
 * @brief Returns the value of str as a C string, terminated with the
 * 	null character. It is the caller's responsibility to free the returned
 * 	string by calling free().
 * @param str the MyString
 * RETURN VALUE:
 *  @return the new string, or NULL if the allocation failed.
 */
char * myStringToCString(const MyString *str)
{
    int length = str->_length;
    char * cStr = (char *) malloc(length * sizeof(char) + 1); // + 1 for '\0'
    if(cStr != NULL)
    {
        memcpy(cStr, str->_sptr, length);
        cStr[length] = '\0';
    }
    return cStr;
}

/**
 * @brief Appends a copy of the source MyString src to the destination MyString dst.
 * @param dest the destination
 * @param src the MyString to append
 * RETURN VALUE:
 *  @return MYSTRING_SUCCESS on success, MYSTRING_ERROR on failure.
 */
MyStringRetVal myStringCat(MyString * dest, const MyString * src)
{
    MyString * temp = myStringClone(dest);
    if(changeMyStringLength(dest, dest->_length + src->_length) == MYSTRING_ERROR)
    {
        return MYSTRING_ERROR;
    }
    memcpy(dest->_sptr, temp->_sptr, temp->_length);
    memcpy(dest->_sptr + temp->_length, src->_sptr, src->_length);
    myStringFree(temp);
    return MYSTRING_SUCCESS;
}

/**
 * @brief Sets result to be the concatenation of str1 and str2.
 * 	result should be initially allocated by the caller.
 * 	result shouldn't be the same struct as str1 or str2.
 * @param str1
 * @param str2
 * @param result
 * RETURN VALUE:
 *  @return MYSTRING_SUCCESS on success, MYSTRING_ERROR on failure.
 */
MyStringRetVal myStringCatTo(const MyString *str1, const MyString *str2, MyString *result)
{
    if(changeMyStringLength(result, str1->_length + str2->_length) == MYSTRING_ERROR)
    {
        return MYSTRING_ERROR;
    }
    memcpy(result->_sptr, str1->_sptr, str1->_length);
    memcpy(result->_sptr + str1->_length, str2->_sptr, str2->_length);
    return MYSTRING_SUCCESS;
}

/**
 * @brief Compare str1 and str2.
 * @param str1
 * @param str2
 *
 * RETURN VALUE:
 * @return an integral value indicating the relationship between the strings:
 * 	A zero value indicates that the strings are equal.
 * 	A value greater than zero indicates that the first character that does not match has a greater ASCII value in str1 than in str2;
 * 	And a value less than zero indicates the opposite.
 */
int myStringCompare(const MyString *str1, const MyString *str2)
{
    int output = 0;
    int shorter = 0;
    
    if(str1 == str2)
    {
        // str1 points to the same struct as str2
        return output;
    }
    shorter = str2->_length;
    if(str1->_length > str2->_length)
    {
        shorter = str2->_length;
        output = 1; // in case all of str2 is equal to str1 but shorter, output will be positive
    }
    else if (str1->_length < str2->_length)
    {
        shorter = str1->_length;
        output = -1;
    }
    int diff = 0;
    for (int i = 0; i < shorter; i++)
    {
        diff = ((int) str1->_sptr[i] - (int) str2->_sptr[i]);
        if (diff != 0)
        {
            return diff;
        }
    }
    return output;
}


/**
 * @brief Compares str1 and str2.
 * @param str1
 * @param str2
 * @param comparator
 * RETURN VALUE:
 * @return an integral value indicating the relationship between the strings:
 * 	A zero value indicates that the strings are equal according to the custom comparator (3rd parameter).
 * 	A value greater than zero indicates that the first MyString is bigger according to the comparator.
 * 	And a value less than zero indicates the opposite.
 */
int myStringCustomCompare(const MyString *str1, const MyString *str2, int (*cmp)(const MyString *, const MyString *))
{
    return cmp(str1, str2);
}

/**
 * @brief Check if str1 is equal to str2.
 * @param str1
 * @param str2
 *
 * RETURN VALUE:
 * @return an integral value indicating the equality of the strings (logical equality - that they are composed of the very same characters):
 * 	A zero value indicates that the strings are not equal.
 * 	A greater than zero value indicates that the strings are equal.
 */
int myStringEqual(const MyString *str1, const MyString *str2)
{
    if(str1 == str2)
    {
        // str1 points to the same struct as str2
        return 0;
    }
    if(str1->_length != str2->_length)
    {
        // string are not of the same length so the aren't equal
        return 1;
    }
    for (int i = 0; i < str1->_length; i++)
    {
        if(str1->_sptr[i] != str2->_sptr[i])
        {
            return 1;
        }
    }
    // no unmatched char, so string are equal
    return 0;
}

/**
 * @brief Check if str1 is equal to str2.
 * @param str1
 * @param str2
 * @param comparator
 *
 * RETURN VALUE:
 * @return an integral value indicating the equality of the strings using a custom comparator (3rd parameter):
 * 	A zero value indicates that the strings are not equal.
 * 	A greater than zero value indicates that the strings are equal.
 */
int myStringCustomEqual(const MyString *str1, const MyString *str2, int (*cmp)(const MyString *, const MyString *))
{
    return cmp(str1, str2);
}

/**
 * @return the amount of memory (all the memory that used by the MyString object itself and its allocations), in bytes, allocated to str1.
 */
unsigned long myStringMemUsage(const MyString *str1)
{
    return sizeof(MyString) + str1->_length;
}

/**
 * @return the length of the string in str1.
 */
unsigned long myStringLen(const MyString *str1)
{
    return str1->_length;
}

/**
 * Writes the content of str to stream. (like fputs())
 *
 * RETURNS:
 *  @return MYSTRING_SUCCESS on success, MYSTRING_ERROR on failure.
 */
MyStringRetVal myStringWrite(const MyString *str, FILE *stream)
{
    if(fputs(myStringToCString(str), stream) == EOF)
    {
        return MYSTRING_ERROR;
    }
    return MYSTRING_SUCCESS;
}

/**
 * @brief sort an array of MyString
 * @param arr
 * @param len
 * @param comparator custom comparator
 *
 * RETURN VALUE:
 * @return a sorted array
 */
MyString * myStringCoustomSort(MyString * arr[], int length, int (*cmp)(const void *, const void *))
{
    qsort(arr, length, sizeof(MyString), cmp);
    return *arr;
}

/**
 * @brief sorts an array of MyString pointers according to the default comparison (like in myStringCompare)
 * @param arr
 * @param len
 *
 * RETURN VALUE: none
 */
void myStringSort(MyString * arr[], int length)
{
    qsort(arr, length, sizeof(MyString), (int(*)(const void*, const void*)) &myStringCompare);
}