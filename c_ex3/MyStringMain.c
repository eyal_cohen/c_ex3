//
//  MyStringMain.c
//

#include <stdio.h>
#include "MyString.h"

#define MSG_1 "Enter string A:\n"
#define MSG_2 "Enter string B:\n"
#define OUT_1 "A is smaller than B"
#define OUT_2 "B is smaller than A"

/**
 * @brief get two strings from user and write a sentence describing their relation to file
 *
 */
int main(int argc, const char * argv[])
{
    char * file = "test.out";
    char * mode = "w+";
    
    char * output;
    
    FILE * fp;
    fp = fopen(file, mode);
    
    char strA[500];
    char strB[500];
    printf(MSG_1);
    fgets(strA, 500, stdin);
    
    printf(MSG_2);
    fgets(strB, 500, stdin);
    
    MyString * str1 = myStringAlloc();
    myStringSetFromCString(str1, strA);
    
    MyString * str2 = myStringAlloc();
    myStringSetFromCString(str2, strB);
    
    int diff = myStringCompare(str1, str2);
    
    if(diff <= 0)
    {
        output = OUT_1;
    }
    else
    {
        output = OUT_2;
    }
    fprintf(fp, output);
    fclose(fp);
    myStringFree(str1);
    myStringFree(str2);
}